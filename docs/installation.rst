Installation instructions
=========================

You can install `transcoded` through the pypi repositories:

.. code-block:: shell-session

    $ sudo pip install python-transcoded

This installs the transcoded daemon and its dependencies. It doesn't include any files for the init system, only the
`transcoded` command that starts the daemon. An example init script for systemd:

.. code-block:: ini

    [Unit]
    Description=Video transcoding daemon

    [Service]
    Type=simple
    ExecStart=/usr/bin/python3 -m transcoded

    [Install]
    WantedBy=multi-user.target

